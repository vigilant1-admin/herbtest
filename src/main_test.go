package GitLabBuildDemo

import "testing"

func TestHello(t *testing.T) {
    got := Hello()
    want := "Hello Gitlab CI!"

    if got != want {
        t.Errorf("got %q want %q", got, want)
    }
}
