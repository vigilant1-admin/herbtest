// This is the standard Vigilant code header
// Legal speak header
// Makeing this header changed to link it to issue #2
package GitLabBuildDemo

import "fmt"

// This function is the initial greeting to the user that prepares them to receive the Best day ever phenomenon 
func Hello() string {
    return "Hello Gitlab CI!"
}

// The GitLabBuildDemo function
func GitLabBuildDemo() {
    fmt.Println(Hello())
}
